<?php
    define("SPOTIFY_CLIENT_ID", "");
    define("SPOTIFY_CLIENT_SECRET", "");
    define("SPOTIFY_GRAND_TYPE", "client_credentials");
    define("SPOTIFY_ACCOUNT_URL", "https://accounts.spotify.com");
    define("SPOTIFY_TOKEN_URL", "https://accounts.spotify.com/api/token");
    define("SPOTIFY_API_URL", "https://api.spotify.com");
    define("SPOTIFY_SEARCH_PATH", "/v1/search");
    define("SPOTIFY_ARTIST_PATH", "/v1/artists");
?>