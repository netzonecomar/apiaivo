<?php 
namespace SpotifyApi;

class Album {

    public $name = null;
    public $released=null;
    public $tracks = null;
    public $cover=array();

    public function __construct( string $name)
    {
        $this->name = $name;
    }

    public function setReleased(string $date)
    {
        $this->released =date_format(date_create($date), 'd-m-Y');
    }

    public function getReleased()
    {
        return   $this->released;
    }

    public function setTracks( int $tracks)
    {
        $this->tracks=$tracks;
    }

    public function getTracks()
    {
        return $this->tracks;
    }

    public function setCover( $cover)
    {
        $this->cover[]=$cover;
    }

    public function getCoverList()
    {
        return $this->cover;
    }


}

?>