<?php 
namespace SpotifyApi;

class Artist {

    public $name = null;
    private $genre=null;
    private $idSpotify=null;
    private $albums=array();

    public function __construct( string $name, string $idSpotify)
    {
        $this->name = $name;
        $this->idSpotify = $idSpotify;
    }

    public function setGenre(string $genreName)
    {
        $this->genre = $genreName;
    }
    
    public function getGenre()
    {
        return   $this->genre;
    }

    public function setAlbum(Album $album)
    {
        $this->albums[]=$album;
    }

    public function getAlbums()
    {
        return $this->albums;
    }

    public function getIdSpotify()
    {
        return $this->idSpotify;
    }

}

?>