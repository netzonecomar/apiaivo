<?php
namespace SpotifyApi;

class SpotifyWebApi extends SpotifySession{
    
    const API_URL = SPOTIFY_API_URL;
    const SEARCH_PATH= SPOTIFY_SEARCH_PATH;
    const ARTIST_PATH= SPOTIFY_ARTIST_PATH;
    
    
    public function __construct()
    {
        $this->credentialsToken();        
    }

    public function searchAlbums(Artist $artist)
    {
        $listAlbums=array();

        if ($this->accessToken) {
            $headers = ['Authorization' => 'Bearer ' . $this->accessToken];
            $url= self::API_URL.self::ARTIST_PATH.'/'.$artist->getIdSpotify().'/albums';
            $parameters['include_groups']='album';
            $method = "GET";
            $response= $this->sendSearch($method,$url,$parameters,$headers);

            //Comienza Parseo de la informacion Recuperada
            if ($response['status']==200) {
                $data = ($response['body']['items'])? $response['body']['items'] : null;
                foreach ($data as $disc) {
                    $album= new Album($disc['name']);
                    $album->setReleased($disc['release_date']);
                    $album->setTracks($disc['total_tracks']);
                    foreach ($disc['images'] as $cover) {
                        $album->setCover($cover);
                    }
                    $artist->setAlbum($album);
                    $listAlbums[]=$album;
                }
            }

        }

        return $listAlbums;
    }

    public function searchArtist(string $name)
    {   
        $artist=null;
        if ($this->accessToken && !(empty($name))) {
            $headers = ['Authorization' => 'Bearer ' . $this->accessToken];
            $url= self::API_URL.self::SEARCH_PATH;
            $parameters['q']=$name;
            $parameters['type']='artist';
            $parameters['limit']=1;
            $method = "GET";
        
            $response= $this->sendSearch($method,$url,$parameters,$headers);
            if ($response['status']==200){
                $data = ($response['body']['artists']['items'])? array_shift($response['body']['artists']['items']) : null;
                if (!empty($data)){
                    $artist=new Artist($data['name'],$data['id']);  
                }  
            }
        }
        return $artist;
    }
    
    private function sendSearch($method, $url, $parameters = [], $headers = [])
    {
        $result= ['body'=>null,'status'=>null];
        $newHeaders = [];
        $method=strtoupper($method);
        $parameters = http_build_query($parameters); 

        foreach ($headers as $key => $val){
            $newHeaders[] = "$key: $val";
        }

        $curloptions = [
            CURLOPT_ENCODING => '',
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => $newHeaders,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url
        ];

        switch ($method){
            case 'GET':
                $curloptions[CURLOPT_CUSTOMREQUEST] = $method;
                if ($parameters) {
                    $url .= '?' . $parameters;
                }
                break ;
            case 'POST':
                $options[CURLOPT_POST] = true;
                $options[CURLOPT_POSTFIELDS] = $parameters;
                break;
        }

        $curloptions[CURLOPT_URL] = $url;

        $curl = curl_init();
            curl_setopt_array($curl, $curloptions);
            $response = curl_exec($curl);

            if (curl_error($curl)){
               throw new \Exception('CURL transport error: ' . curl_errno($curl) . ' ' .  curl_error($curl), 400);
            }

            list($headers, $body) = explode("\r\n\r\n", $response, 2);

            if (preg_match('/^HTTP\/1\.\d 200 Connection established$/', $headers) === 1) {
                list($headers, $body) = explode("\r\n\r\n", $body, 2);
            }

            $result['body']=json_decode($body,true);
            $result['status']=(int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
            
            
            if (isset($result['body']['error'])){
                if (is_array($result['body']['error'])){
                    throw new \Exception($result['body']['error']["message"], $result['status']);
                }else{
                    throw new \Exception($result['body']['error'], $result['status']);
                }
            }   

        curl_close($curl);

        return $result;
    }

    
}

?>