<?php 
namespace SpotifyApi;

require_once('./config.php');

class SpotifySession{

    const ACCOUNT_URL = SPOTIFY_ACCOUNT_URL;
    const TOKEN_URL= SPOTIFY_TOKEN_URL;

    private $clientId = SPOTIFY_CLIENT_ID;
    private $clientSecret = SPOTIFY_CLIENT_SECRET;
    private $grand_type= SPOTIFY_GRAND_TYPE;
    protected $refreshToken = '';
    protected $expirationTime = 0;
    protected $scope = null;
    public $accessToken = '';
    

    protected function credentialsToken()
    {
       
        $parameters = [
            'grant_type' => $this->getGrandType() ,
        ];
        $parameters = http_build_query($parameters);
        $headers[] = 'Authorization: Basic ' . base64_encode($this->getClientId() . ':' . $this->getClientSecret());

        $curloptions = [
            CURLOPT_ENCODING => '',
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $parameters,
            CURLOPT_URL => self::TOKEN_URL
        ];

        $curl = curl_init();  
            curl_setopt_array($curl, $curloptions);
            $response = curl_exec($curl);

            if (curl_error($curl)) {
                throw new \Exception('CURL transport error: ' . curl_errno($curl) . ' ' .  curl_error($curl));
            }
           
            list($headers, $body) = explode("\r\n\r\n", $response, 2);

            if (preg_match('/^HTTP\/1\.\d 200 Connection established$/', $headers) === 1) {
                list($headers, $body) = explode("\r\n\r\n", $body, 2);
            }

            $result['body']=json_decode($body,true); 
            $result['headers']=$headers;
            $result['status']=(int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        if (isset($result['body']['error'])) {
            throw new \Exception($result['body']['error'], $result['status']);
        }

        if ($result['status'] == 200){
            $this->accessToken = $result['body']['access_token'];
            $this->expirationTime = time() + $result['body']['expires_in'];
            return true;
        }

        return false;
    }

    private function getGrandType()
    {
        return $this->grand_type;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    private function getClientSecret()
    {
        return $this->clientSecret;
    }

    protected function getAccessToken()
    {
        return $this->accessToken;
    }

    
}
?>