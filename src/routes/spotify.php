<?php
use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SpotifyApi\SpotifyWebApi;
use SpotifyApi\SpotifySession;
use SpotifyApi\Album;
use SpotifyApi\Artist;

//use GuzzleHttp\Client;


return function (App $app) {
    
    $app->get('/album', function (Request $request, Response $response, array $args)
    {        
        $listAlbums=array();
        
        try {

            $name = ($request->getParam('q'))? $request->getParam('q') : '';
            $search = new SpotifyWebApi(); 
            $artist= $search->searchArtist($name);
            if (!empty($artist)){
                $listAlbums= $search->searchAlbums($artist);
            }

        }catch (Exception $e){
            return $response->withJson($e->getMessage(), $e->getCode());
        }

        return $response->withJson($listAlbums, 200);
        //return $search;
    });
};