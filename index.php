<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require 'vendor/autoload.php';
$settings = require 'settings.php';

$app = new  \Slim\App($settings);

$app->group('/api/v1', function () use ($app){

    $spotify = require 'src/routes/spotify.php';
    $spotify($app);

});

$app->run();